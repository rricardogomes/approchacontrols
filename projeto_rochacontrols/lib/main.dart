import 'dart:async';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:simple_permissions/simple_permissions.dart';
import 'HttpRequests.dart' as RequestHttp;

void main() => runApp(MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Leitor QR',
      home: InicialPage(),
    ));

class InicialPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new InicialPageState();
  }
}

class InicialPageState extends State<InicialPage> {
  String resultadoLeitura = "";
  Permission permission = Permission.Camera;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("QR Scanner"),
        backgroundColor: Colors.green[800],
      ),
      body: Center(
        child: Text(
          resultadoLeitura,
          style: new TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        icon: Icon(
          Icons.camera_alt,
          textDirection: TextDirection.ltr,
        ),
        label: Center(
          child: Column(
            children: <Widget>[
              Text("Leitor QR"),
            ],
          ),
        ),
        backgroundColor: Colors.green[400],
        //onPressed: scannerQR,
        onPressed: RequestHttp.recuperarDados,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }

  requestPermission() async {
    await SimplePermissions.requestPermission(permission);
  }

  Future scannerQR() async {
    try {
      String qrResult = await BarcodeScanner.scan();
      setState(() {
        resultadoLeitura = qrResult;
      });
    } on PlatformException catch (e) {
      if (e.code == BarcodeScanner.CameraAccessDenied) {
        requestPermission();
      } else {
        setState(() => resultadoLeitura = "Erro desconhecido $e.");
      }
    } on FormatException {
      setState(() => resultadoLeitura = "Não foi possivel realizar a leitura.");
    } catch (e) {
      setState(() => resultadoLeitura = "Erro desconhecido $e.");
    }
  }
}