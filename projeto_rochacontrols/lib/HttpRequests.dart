import 'package:http/http.dart' as http;
//import 'dart:convert' as convert;

var client = new http.Client();
var baseUrl = 'http://localhost:52402/api/codigoqr';

Future<String> recuperarDados() async {
  try {
    http.Response response =
        await client.get(baseUrl, headers: {"Accept": "application/json"});
    print(response.body);
  } finally {
    client.close();
  }
}